import socket

class WebApp:
    def parse(self, request):
        received = request.split()[1]
        return {'received': received}

    def process(self, analyzed):
        http = "200 OK"
        html = "<html><body><h1>Hello World!</h1></body></html>"
        return http, html

    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind((self.host, self.port))
        self.socket.listen(5)

    def accept_clients(self):
        while True:
            print("Waiting for connections...")
            (client_socket, client_address) = self.socket.accept()
            request = client_socket.recv(1024).decode('utf-8')
            if not request:
                continue
            parsed_request = self.parse(request)
            http, html = self.process(parsed_request)
            response = f"HTTP/1.1 {http}\r\n\r\n{html}"
            client_socket.sendall(response.encode('utf-8'))
            client_socket.close()
